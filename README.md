# ElasticSearch6xDemo

#### 项目介绍
ElasticSearch 6.2.4示例代码，供参考

#### 软件架构
软件架构说明


#### 安装教程

1. 直接下载代码，使用intellij IDEA导入即可
2. 需要修改的各自集群配置在Constants中修改即可

#### 使用说明

1. Test.CreateIndex 为创建索引的相关示例，其中有较为详细的注释
2. MainMethod 为搜索的相关示例，都是属于较为简单的示例，相信大家都能看懂。

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)