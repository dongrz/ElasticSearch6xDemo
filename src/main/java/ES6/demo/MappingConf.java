package ES6.demo;

import org.elasticsearch.common.xcontent.XContentBuilder;

import java.io.IOException;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

/**
 * 匹配方法，主要是为了不同的字段使用不同的analyzer，尤其是中文。
 * Created by dong on 2018/4/25.
 */
public class MappingConf {

    /**
     * 针对需要创建索引的属性的相关的配置
     * @return
     */
    public static XContentBuilder getMapping(){
        XContentBuilder mapping = null;
        try {
            mapping = jsonBuilder()
                    .startObject()
                    //关闭TTL
//                    .startObject("_ttl")
//                    .field("enabled",false)
//                    .endObject()
                    .startObject("properties")
                        .startObject("title")
                            .field("type","text")
                            .field("boost", 8.0)
                            //指定index analyzer 为 ik
                            .field("analyzer", "ik_max_word")
                            //指定search_analyzer 为ik_syno
                            .field("search_analyzer", "ik_max_word")
                        .endObject()
//                        .startObject("title")
//                            .field("type","text")
//                            .field("boost", 8.0)
//                            //指定index analyzer 为 ik
//                            .field("analyzer", "standard")
//                            //指定search_analyzer 为ik_syno
//                            .field("search_analyzer", "standard")
//                        .endObject()
                        .startObject("content")
                            .field("type","text")
                            .field("analyzer","ik_max_word")
                            .field("search_analyzer","ik_smart")
                        .endObject()
                    .endObject()
                    .endObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mapping;
    }


    public static XContentBuilder getMapping2(){
        XContentBuilder mapping = null;
        try {
            mapping = jsonBuilder().startObject()//.startObject("_ttl").field("enabled",false).endObject()
                    .startObject("properties")
                    .startObject("title")
                    .field("type","text")
                    .field("analyzer","ik_max_word")
                    .field("search_analyzer","ik_max_word")
                    .endObject()

//                        .startObject("age").field("type","long").endObject()
//                        .startObject("date").field("type","date").endObject()
//                        .startObject("message").field("type","keyword").field("index","true").endObject()
//                        .startObject("tel").field("type","keyword").endObject()
//                        .startObject("attr_name").field("type","keyword").field("index","true").endObject()
                    .endObject()
                    .endObject();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mapping;
    }
}
