package ES6.demo.Test;

import ES6.demo.SearchUtils;

import java.util.List;
import java.util.Map;

/**
 * Created by dong on 2018/4/23.
 */
public class MainMethod {

    public static void main(String[] args) {

        //查询关键字
        String text = "标题测试描";

        int querySize = 5;
        int offset = 0;

        //需要搜索的所有字段
        String[] fieldNames = {"name", "title", "content", "author", "appDesc", "appName", "appContent"};

        //TODO 第一种方法，直接获取列表
        List<Map<String, Object>> lists = SearchUtils.searchForList(CreateIndex.indexName, text, querySize, offset, fieldNames);
        for (Map<String, Object> map: lists) {
            for (String key : map.keySet()) {
                System.out.println(key + "=" + map.get(key));
            }
            System.out.println("--------------------");
        }

        //TODO 第二种方法，获取命中列表
//        SearchHits hits = SearchUtils.searchForHits(indexName, text, querySize, offset, fieldNames);
//
//        for (SearchHit hit : hits) {
//            System.out.println("source:" + hit.getSourceAsString());
//            System.out.println("index:" + hit.getIndex());
//            System.out.println("type:" + hit.getType());
//            System.out.println("id:" + hit.getId());
//            //遍历文档的每个字段，高亮显示处理
//            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
//            Map<String, Object> source = hit.getSourceAsMap();
//            //处理高亮 获取高亮字符串
//            if (highlightFields != null && highlightFields.size() != 0){
//                String[] needHighLightFields = fieldNames;
//                for (String needHighLightField : needHighLightFields){
//                    HighlightField titleField = highlightFields.get(needHighLightField);
//                    if(titleField != null){
//                        Text[] fragments = titleField.fragments();
//                        if (fragments != null && fragments.length != 0){
//                            StringBuilder name = new StringBuilder();
//                            for (Text fragment : fragments) {
//                                name.append(fragment);
//                            }
//                            source.put(needHighLightField, name.toString());
//                        }
//                    }
//                }
//            }
//
//            for (String key : source.keySet()) {
//                System.out.println(key + "=" + source.get(key));
//            }
//            System.out.println("--------------------");
//        }
    }

}
