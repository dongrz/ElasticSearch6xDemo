package ES6.demo.Test;

import ES6.demo.ESUtils;
import ES6.demo.IndexUtils;
import ES6.demo.MappingConf;
import ES6.demo.News;
import com.alibaba.fastjson.JSON;

import java.util.Calendar;

/**
 * 创建索引的测试类
 * Created by dong on 2018/4/25.
 */
public class CreateIndex {

    public static final String indexName = "app_index9";
    public static void main(String[] args) {

        // /////  1. 删除原有的索引，否则创建会出错
//        ESUtils.deleteIndex(indexName);

        // /////  2. 创建名为 "appIndex" 的索引，并设置分片为3份，副本为1份
//        boolean createResult = ESUtils.createIndex(indexName, 3, 1);
//        System.out.println("创建索引：" + createResult);

        // /////  3. 创建Mapping，否则analyzer不会生效
        ESUtils.setMapping(indexName, "news", MappingConf.getMapping());

        // /////4. 创建特定内容的索引
        for (int i = 0; i < 10; i++) {
            News news = new News();
            news.setAuthor("作者" + i);
            news.setContent("这是一段测试的描述" + i);
            news.setPostTime(Calendar.getInstance().getTime());
            news.setTitle("标题标题标题" + i);
            news.setUrl("www.baidu.com/" + i);
            String jsonStr = JSON.toJSONString(news);
            //两种结构的方式均可
//          Map json = (Map)JSON.parse(jsonStr);

            IndexUtils.addIndex(indexName, "news", "news" + i, jsonStr);
        }

    }
}
