package ES6.demo;

/**
 * Created by dong on 2018/4/24.
 */
public class Constants {

    //集群名,默认值elasticsearch
    public static final String CLUSTER_NAME = "myApp";

    //ES集群中某个节点
    public static final String HOSTNAME = "node1";

    //连接端口号
    public static final int TCP_PORT = 9300;

    //高亮显示前置
    public static final String PRE_TAGS = "<em>";

    //高亮显示后置
    public static final String LAST_TAGS = "</em>";

}
