package ES6.demo;

import java.util.Date;

/**
 * 基础对象类
 * Created by dong on 2018/4/23.
 */
public class News {
    private String title;
    private String author;
    private String content;
    private String url;
    private Date postTime;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getPostTime() {
        return postTime;
    }

    public void setPostTime(Date postTime) {
        this.postTime = postTime;
    }

    public News(String title, String author, String content, String url, Date postTime) {
        this.title = title;
        this.author = author;
        this.content = content;
        this.url = url;
        this.postTime = postTime;
    }

    public News(){

    }
}
